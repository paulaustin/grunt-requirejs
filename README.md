# Getting started with Grunt #

See the [Getting Started](http://gruntjs.com/getting-started "Getting started") instructions on gruntjs.com.

Grunt is dependent on [Node.js](http://nodejs.org/ "Node.js") and uses the [Node Package Manager](https://npmjs.org/ "Node Package Manager") so download and install Node.js if you don't already have it.

Checkout the Bitbucket repo, open a Command Line window and navigate to the project root folder. You can now install all the required plugins for this project by running `npm install`. This reads all the package dependencies in the package.json file and installs them all. You should now have a node_modules folder containing all the Grunt plugin packages required.

## The Gruntfile ##

The `Gruntfile.js` is a valid JavaScript file that lives in the root folder of your project, next to the package.json file, and should be committed to Git along with your project source. 

The Gruntfile comprises the following sections:

- The "wrapper" function
- Project and task configuration (the largest section of the file)
- Loading grunt plugins and tasks
- Defining default and custom tasks

See each plugin's NPM page for configuration options and usage examples.

*The package.json file should be committed to Git along with the project source.*

## Using Grunt tasks ##

Run Grunt tasks from the Command Line. Each Gruntfile has a default task that you can use by simply running 'grunt'. You can run individually defined tasks by running 'grunt [taskname]' e.g. 'grunt compass' or 'grunt clean'. Lastly you can define multi-step tasks which are run in the same manner e.g. 'grunt require' runs jshint follwed by requirejs.