//
// Boot strap the application only if the browser passes the HTML5 test
// i.e. "cut's the mustard".
//
// http://responsivenews.co.uk/post/18948466399/cutting-the-mustard
//

if ( 'querySelector' in document && 'addEventListener' in window ) { 
  
    console.log("This browser 'cuts the mustard'...so bootstrap the JavaScript");
    // Bootstrap the JavaScript Application

    require.config({
        baseUrl: 'assets/js',
        paths: {
            
            // libs
            'jquery'     : 'libs/jquery',
            'parsley'    : 'libs/parsley',
            'modernizr'  : 'libs/modernizr',
            
            //modules
            'module-a'   : 'modules/module-a',
            'module-b'   : 'modules/module-b',
            'module-c'   : 'modules/module-c',
            'module-d'   : 'modules/module-d'
        }
    });

    require(['module-a']);
    require(['module-b']);
    require(['module-c']);
    require(['module-d']);
}