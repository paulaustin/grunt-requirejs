# Compass configuration file.

project_path = "assets/"

# Important! change the paths below to match your project setup

css_dir = "css" # update to the path of your css files.
sass_dir = "sass" # update to the path of your sass files.
images_dir = "img" # update to the path of your image files.
javascripts_dir = "js" # update to the path of your script files.

output_style = (environment == :production) ? :compressed : :expanded
color_output = false # required for Mixture