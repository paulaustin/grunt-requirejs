module.exports = function(grunt) {

    /*
        Grunt installation:
        -------------------
            npm install -g grunt-cli
            npm install -g grunt-init
            npm init (creates a `package.json` file)

        Project Dependencies:
        ---------------------
            npm install grunt-contrib-clean --save-dev
            npm install grunt-contrib-compass --save-dev
            npm install grunt-contrib-cssmin --save-dev
            npm install grunt-contrib-jshint --save-dev
            npm install grunt-contrib-uglify --save-dev
            npm install grunt-contrib-requirejs --save-dev
    */

    // Project configuration.
    grunt.initConfig({

        // Store your Package file so you can reference its specific data
        // whenever necessary. If the project has already been setup with
        // Grunt, then running 'npm install' on the CMD line from the project
        // root folder, (where the package.json and Gruntfile.js live) will
        // install all the plugin dependencies in the package.json file.
        pkg: grunt.file.readJSON('package.json'),


        // Grunt task definition to clean the build folder
        clean: {
            build: {
                src: ["assets/build"]
            }
        },

        compass: {
            build: {
                options: {
                    config: '.config.rb'
                }
            }
        },

        // Grunt task definition to minify the compiled single CSS file output by Sass
        cssmin: {
            combine: {
                options: {
                    banner: '/*! <%= pkg.name %> v<%= pkg.version %> :: <%= grunt.template.today("dd-mm-yyyy") %> :: Production CSS */'
                },
                files: {
                    'assets/build/enhanced.min.css': ['assets/css/enhanced.css'],
                    'assets/build/core.min.css': ['assets/css/core.css']
                }
            }
        },

        // Grunt task definition to lint all JS modules
        jshint: {
            files: ['assets/js/modules/**/*.js'],
            options: {
                // options here to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },

        // Compress the bootstrap javascript
        uglify: {
            build: {
                files: {
                    'assets/build/bootstrap.min.js': ['assets/js/bootstrap.js']
                }
            }
        },

        // Grunt task definition to optimise RequireJS and all modules and dependencies
        requirejs: {
            buildjs: {
                options: {

                    // All files are located relative to this path
                    // (except the mainConfigFile)
                    baseUrl: 'assets/js/',

                    // Location of the build config.
                    // Note this needs the full path
                    // and the .js filename suffix - 
                    // this is not relative to baseUrl 
                    mainConfigFile: 'assets/js/script.js',

                    // The name of this module which gets
                    // appended to the bottom of the optimised
                    // file:
                    // define("script", function(){});
                    name: 'script',

                    // Single file output by the r.js optimisation process
                    out: 'assets/build/scripts.min.js',

                    // To stop minification, uncomment the next line.
                    //optimize: 'none',

                    // Remove license comments in a minified file
                    // Without this line, the license comments
                    // will be preserved by default.
                    preserveLicenseComments: false,

                    // The following lines are required to include
                    // require.js within the optimised output so that
                    // there is no need to load it via a separate
                    // http request
                    paths: {
                        requireLib: 'libs/require'
                    },
                    include: 'requireLib'
                }
            }
        }
    });


    // Load NPM Tasks
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-requirejs');


    // Default task
    // Simply run using 'grunt' at the command line.
    grunt.registerTask("default", ['clean', 'compass', 'cssmin', 'jshint', 'uglify', 'requirejs']);

    // Add further registered tasks as required, e.g.
    grunt.registerTask("require", ['jshint', 'requirejs']);

    // You can run any individual task by using 'grunt [taskname]'
    // e.g. 'grunt clean' or 'grunt requirejs' etc.
};